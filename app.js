var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
const _ = require('lodash');
let mockdata = require('./mockdata.json');
const moment = require('moment');
// const router = express.Router();
var app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
let sortedData = _.sortBy(mockdata, ["pk"]);
let date = new Date();
sortedData = sortedData.map(function (value) {
  console.log(moment(date).add(value.fields.epoch_time_start, 's').add(10, 'h').toISOString());
  value.fields.epoch_time_start = moment(date).add(value.fields.epoch_time_start, 's').toISOString();
  console.log(moment(date).add(value.fields.epoch_time_end, 's').add(10, 'h').toISOString());
  value.fields.epoch_time_end = moment(date).add(value.fields.epoch_time_end, 's').toISOString();
  return value;
});

app.use('/', (function (req, res, next) {
  req.query.start = new Date(req.query.start);
  req.query.end = new Date(req.query.end);
  let data = _.clone(sortedData);
  data = data.filter(function (value) {
    // console.log("req.query.start :", req.query.start);
    // console.log(req.query.start < value.fields.epoch_time_start);
    // console.log("value.fields.epoch_time_start :", value.fields.epoch_time_start);
    // console.log("req.query.end :", req.query.end);
    // console.log(req.query.start < value.fields.epoch_time_start & req.query.end > value.fields.epoch_time_start);
    // console.log(moment(value.fields.epoch_time_start).isBetween(req.query.start, req.query.end), "using moment");
    // console.log(req.query.end > value.fields.epoch_time_start);
    return moment(value.fields.epoch_time_start).isBetween(req.query.start, req.query.end);
  });
  // console.log(data);
  res.json(data);
}));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.log(err);
  res.json(err);
});

module.exports = app;